/**
 * Created by yaroslavo on 11/8/14.
 */

$.fn.exists = function () {
    return this.length !== 0;
}

function YPlayer($holder, customSettings) {


    // private
    var core;
    var isPlaying = false;

    var settings = {
        element: null,
        user: 'viewthesource',
        playlist: 'RDHCa43uN61NhBk',
        fullscreen: false,
        accent: '#fff',
        controls: true,
        annotations: false,
        autoplay: false,
        chainVideos: true,
        browsePlaylists: true,
        wmode: 'opaque',
        events: {
            videoReady: function(){},
            stateChange: function(){}
        }
    };

    function initializePlayer() {
        core = new YTV('frame', (settings));

    }

    //constructor
    initializePlayer();

    // public
    return {
        core: core,
        settings: settings,

        next: function () {
            console.log('next song');
            if (!$('.ytv-active').next().exists()) {
                $('.ytv-active').parent('ul').children().first().children()[0].click()
            }else{
                $('.ytv-active').next().children()[0].click();
            }
        },

        pause: function () {
            if ( isPlaying ) {
                console.log('paused');
                globalPlayer.stopVideo();
                isPlaying = false;
            } else {
                console.log('play!');
                globalPlayer.playVideo();
                isPlaying = true;
            }
        },
        previous: function () {
            console.log('previous');
            if (!$('.ytv-active').prev().exists()) {
                $('.ytv-active').parent('ul').children().last().children()[0].click()
            }else{
                $('.ytv-active').prev().children()[0].click();
            }

        }
    }

}



