$(document).ready(function(){
    var yplayer;
    var customSettings = {
        element: null,
        user: 'viewthesource',
        playlist: 'RDHCa43uN61NhBk',
        fullscreen: false,
        accent: '#fff',
        controls: true,
        annotations: false,
        autoplay: false,
        chainVideos: true,
        browsePlaylists: true,
        wmode: 'opaque',
        events: {
            videoReady: function(){},
            stateChange: function(){}
        }
    };

    yplayer = new YPlayer($('#frame'), customSettings);
    $('.start-stop').on('click', function() {
        yplayer.pause();
    })
});